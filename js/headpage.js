$(document).ready(function(){
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false
    });

    //热门游戏
    var htmlgame = '';
    var listlesson = [];
    listlesson = [
        {
            pageid:'sy',
            lessonid:'zmyk',
            lessonimg:'images/classzhangmen.png',
            lessonname:'掌门优课',
            lessontitle:'暑期逆袭提分班'
        },
        {
            pageid:'sy',
            lessonid:'zyb',
            lessonimg:'images/classzuoye.png',
            lessonname:'作业帮',
            lessontitle:'小中高特训营'
        },
        {
            pageid:'sy',
            lessonid:'zmydy',
            lessonimg:'images/zhangmen.png',
            lessonname:'掌门一对一',
            lessontitle:'全科专题突破，真人互动教学'
        },
        {
            pageid:'sy',
            lessonid:'qq',
            lessonimg:'images/qingqing.png',
            lessonname:'轻轻教育',
            lessontitle:'课前咨询诊断，课后跟踪调查'
        },
        // {
        //     pageid:'sy',
        //     lessonid:'qmkc',
        //     lessonimg:'images/classhetao.png',
        //     lessonname:'启蒙课程',
        //     lessontitle:'世界机器人冠军，电脑上课'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'htbc',
        //     lessonimg:'images/classhetao.png',
        //     lessonname:'核桃编程',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'hmbc',
        //     lessonimg:'images/classhema.jpg',
        //     lessonname:'和码编程',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'pxwx',
        //     lessonimg:'images/classpuxin.png',
        //     lessonname:'朴新网校',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'ydk',
        //     lessonimg:'images/classyoudao.png',
        //     lessonname:'有道精品课',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'xes',
        //     lessonimg:'images/classxueersi.png',
        //     lessonname:'学而思',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'yfd',
        //     lessonimg:'images/classyuanfudao.png',
        //     lessonname:'猿辅导',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // },
        // {
        //     pageid:'sy',
        //     lessonid:'zyb',
        //     lessonimg:'images/classzuoye.png',
        //     lessonname:'作业帮',
        //     lessontitle:'强化基础思维能力，培养高效学习方法'
        // }
    ];    

    if(listlesson.length<1){
    }else{
        for (var i = 0; i < listlesson.length; i++) {
            htmlgame += 
                '<div class="gameBox">'+
                    '<div class="gameImg"><img src="'+listlesson[i].lessonimg+'" /></div>'+
                    '<div class="theIcoBox">'+
                        '<div class="thehottop">热</div>'+
                        '<div class="theHotBox">'+
                            '<div class="san"></div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="gamecenBox">'+
                        '<h4>'+listlesson[i].lessonname+'</h4>'+
                        '<p>'+listlesson[i].lessontitle+'</p>'+
                    '</div>'+
                    '<div class="gameBtnBox">'+
                        '<button onclick="enterLesson(\''+listlesson[i].lessonid+'\',\''+listlesson[i].pageid+'\')"><i></i>进入</button>'+
                    '</div>'+
                '</div>'
            
        }
        $(".hotGameBox").html(htmlgame);
    };


    //点击搜索去搜索页
    // $('.navBoxRight').click(function(){
    //     location.href='searchpage.html'
    // })

    //点击重新加载
    // $('.reload').click(function(){
    //     alert("测试")
    // })
    
})
function toinformation(even){
    console.log(even)
    location.href='information.html?newpage='+even;
}