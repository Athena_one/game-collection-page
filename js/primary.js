$(document).ready(function(){

    //获取页面参数
    var theurl=location.href
    var thedata = theurl.split('?class=')[1];
    var html = '';
    var list = [];
    if(thedata == 1){//语文
        $(".primaryTitle").css("background","#f6b37f")
        $('.primaryName').text("语文")
        
        //语文数据赋值        
        list = [
            {
                pageid:'yuwen',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'yuwen',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }
    if(thedata == 2){ //数学
        $(".primaryTitle").css("background","#555ac0")
        $(".primaryName").text("数学");
         //数学数据赋值        
         list = [
            {
                pageid:'shuxue',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'shuxue',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }
    if(thedata == 3){//英语
        $(".primaryTitle").css("background","#8a8ddc")
        $(".primaryName").text("英语");
         // 英语数据赋值        
         list = [
            {
                pageid:'yingyu',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'yingyu',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }
    if(thedata == 4){ //化学
        $(".primaryTitle").css("background","#93dcfd")
        $(".primaryName").text("化学");
         //化学数据赋值        
         list = [
            {
                pageid:'huaxue',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'huaxue',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }
    if(thedata == 5){//物理
        $(".primaryTitle").css("background","#ff7170")
        $(".primaryName").text("物理");
        //物理数据赋值        
        list = [
            {
                pageid:'wuli',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'wuli',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }
    if(thedata == 6){//其他
        $(".primaryTitle").css("background","#85db7a")
        $(".primaryName").text("其他");
        //其他数据赋值        
        list = [
            {
                pageid:'qita',
                lessonid:'qmkc',
                lessonimg:'images/gameone.png',
                lessonname:'启蒙课程',
                lessontitle:'世界机器人冠军，电脑上课'
            },
            {
                pageid:'qita',
                lessonid:'htbc',
                lessonimg:'images/gameone.png',
                lessonname:'核桃编程',
                lessontitle:'强化基础思维能力，培养高效学习方法'
            }
        ]
        console.log(list)
        if(list.length!=""){
            for (var i = 0; i < list.length; i++) {
                html += '<div class="gameOne">'+
                        '<i><img src="'+list[i].lessonimg+'" /></i>'+
                        '<p>'+list[i].lessonname+'</p>'+
                        '<button onclick="enterLesson(\''+list[i].lessonid+'\',\''+list[i].pageid+'\')">开始</button>'+
                        '</div>'
            }
        }else{
            html = '<p style="text-align:center">暂无开放项目</p>'
        }
        
        $('.gameBox').html(html);
    }

    //点击重加载
    $('.reload').click(function(){
        alert('测试重加载')
    })

   
})